#include <log.h>
#include "SimulationSluice.h"

SimulationSluice::SimulationSluice() : isRunning(true) { runner = std::thread(&SimulationSluice::Run, this); }

SimulationSluice::~SimulationSluice() {
  isRunning = false;
  actionCV.notify_all();
  if (runner.joinable()) {
    runner.join();
  }
}

// All external actions merely set the switch for what the next Run() should be doing
void SimulationSluice::Start() { StartAction(SluiceAction::STARTING); }
void SimulationSluice::StartEmergency() { StartAction(SluiceAction::EMERGENCY_STOPPING); }
void SimulationSluice::EndEmergency() { StartAction(SluiceAction::EMERGENCY_RESUMING); }
void SimulationSluice::Lower() { StartAction(SluiceAction::LOWERING); }
void SimulationSluice::Raise() { StartAction(SluiceAction::RAISING); }
void SimulationSluice::Open() { StartAction(SluiceAction::OPENING); }
void SimulationSluice::Close() { StartAction(SluiceAction::CLOSING); }
void SimulationSluice::AllowEntry() { StartAction(SluiceAction::ALLOWING_ENTRY); }
void SimulationSluice::AllowExit() { StartAction(SluiceAction::ALLOWING_EXIT); }

void SimulationSluice::BindActions() {
  actions[SluiceAction::STARTING] = std::bind(&SimulationSluice::ActionStarting, this);
  actions[SluiceAction::OPENING] = std::bind(&SimulationSluice::ActionOpening, this);
  actions[SluiceAction::CLOSING] = std::bind(&SimulationSluice::ActionClosing, this);
  actions[SluiceAction::EMERGENCY_STOPPING] = std::bind(&SimulationSluice::ActionEmergencyStopping, this);
  actions[SluiceAction::EMERGENCY_RESUMING] = std::bind(&SimulationSluice::ActionEmergencyResuming, this);
  actions[SluiceAction::LOWERING] = std::bind(&SimulationSluice::ActionLowering, this);
  actions[SluiceAction::RAISING] = std::bind(&SimulationSluice::ActionRaising, this);
  actions[SluiceAction::ALLOWING_EXIT] = std::bind(&SimulationSluice::ActionAllowingExit, this);
  actions[SluiceAction::ALLOWING_ENTRY] = std::bind(&SimulationSluice::ActionAllowingEntry, this);
  // We don't add any action for SluiceAction::IDLE
}

void SimulationSluice::StartAction(SluiceAction::SluiceAction action) {
  std::unique_lock<std::mutex> lock(actionMtx);
  if (currentAction == SluiceAction::EMERGENCY_STOPPING && action != SluiceAction::EMERGENCY_RESUMING) {
    throw std::invalid_argument("Emergency state needs to be ended before another action can start");
  }
  previousAction = currentAction;
  currentAction = action;
  actionCV.notify_one();
}

void SimulationSluice::EndAction(SluiceAction::SluiceAction action) {
  std::unique_lock<std::mutex> lock(actionMtx);
  if (action == currentAction) {
    // If a new action was declared before this one finished, it should not be overwritten
    currentAction = SluiceAction::IDLE;
  }
}

void SimulationSluice::Run() {
  while (isRunning) {
    SluiceAction::SluiceAction action = SluiceAction::IDLE;

    {  // keep scope of mutex small
      std::unique_lock<std::mutex> lock(actionMtx);
      auto checker = [this] { return !isRunning || currentAction != SluiceAction::IDLE; };
      if (!checker()) {
        actionCV.wait(lock, checker);
      }
      action = currentAction;  // copy currentAction to avoid race conditions
    }

    try {
      auto itAction = actions.find(action);
      if (itAction != actions.end() && isRunning) {
        itAction->second();  // do function associated with action
      }
    } catch (std::exception& ex) {
      LOG(ERROR) << "Exception in sluice action: " << ex.what();
      StartAction(SluiceAction::EMERGENCY_STOPPING);
    }
  }
  LOG(INFO) << "Sluice runner exiting now...";
}

WaterLevel::WaterLevel SimulationSluice::GetWaterLevel() {
  Command cmd;
  cmd.AddCmd("GetWaterLevel");
  sim->DoCommand(&cmd);
  return WaterLevel::FromString(cmd.GetResult());
}

void SimulationSluice::ActionStarting() {
  // In normal operation, close door -> change water level -> open other door are continuous actions
  // So that means if we're starting, we want to fall in that loop again
  // Normal behavior is that if we're starting while water is either low or high, and both doors are closed
  // We start with opening the door, not with changing water level

  WaterLevel::WaterLevel water = GetWaterLevel();
  switch (water) {
    case WaterLevel::LOW:
    case WaterLevel::HIGH: {
      activeDoor = (water == WaterLevel::LOW) ? DoorSide::LEFT : DoorSide::RIGHT;
      doors[activeDoor]->CheckClosed() ? Open() : Close();
      break;
    }
    case WaterLevel::BELOW_VALVE_2:
    case WaterLevel::ABOVE_VALVE_2:
    case WaterLevel::ABOVE_VALVE_3: {  // Waterlevel is somewhere in the middle
                                       // Switch direction
      if (doors[activeDoor]->Close()) {
        (activeDoor == DoorSide::LEFT) ? Raise() : Lower();
      }
      break;
    }
    default: {
      throw std::invalid_argument("Invalid water level");
      break;
    }
  }
}

void SimulationSluice::ActionOpening() {
  bool isOk = doors[activeDoor]->Open();
  if (isOk) {
    // Opening the door is the last part of the close -> water change -> open cycle
    EndAction(SluiceAction::OPENING);
  }
}

void SimulationSluice::ActionClosing() {
  bool isOk = doors[activeDoor]->Close();
  if (isOk) {
    // We're in the middle of the close -> water -> open loop
    // We just closed, so now it's time for the water level to change
    (activeDoor == DoorSide::LEFT) ? Raise() : Lower();
  }
}

void SimulationSluice::ActionLowering() {
  activeDoor = DoorSide::LEFT;
  bool isOk = doors[activeDoor]->Lower();
  if (isOk) {
    // Valves are always operated on the same door that should open afterwards
    Open();
  }
}

void SimulationSluice::ActionRaising() {
  activeDoor = DoorSide::RIGHT;
  bool isOk = doors[activeDoor]->Raise();
  if (isOk) {
    // Valves are always operated on the same door that should open afterwards
    Open();
  }
}

void SimulationSluice::ActionAllowingExit() {
  bool isOk = doors[activeDoor]->AllowExit();
  if (isOk) {
    // Allowing exit is not part of an action chain
    EndAction(SluiceAction::ALLOWING_EXIT);
  }
}

void SimulationSluice::ActionAllowingEntry() {
  bool isOk = doors[activeDoor]->AllowEntry();
  if (isOk) {
    // Allowing entry is not part an action chain
    EndAction(SluiceAction::ALLOWING_ENTRY);
  }
}

void SimulationSluice::ActionEmergencyStopping() {
  bool stopped = true;
  for (auto& doorEntry : doors) {
    stopped = doorEntry.second->EmergencyStop() && stopped;
  }
  // Emergency state can only be stopped by emergency resume
}

void SimulationSluice::ActionEmergencyResuming() {
  // Activity should resume after calling Start() again
  // We're just clearing the emergency state
  EndAction(SluiceAction::EMERGENCY_RESUMING);
}
