#include <SSTR.h>
#include <SluiceSim.h>
#include "SimulationValve.h"

SimulationValve::SimulationValve(int nr, DoorSide::DoorSide side)
    : valveNumber(nr), doorSide(side), doorSideStr(DoorSide::ToString(side)), valveNumberStr(std::to_string(nr)) {}

bool SimulationValve::Open() {
  bool valveOk = CheckValveOpen();
  if (!valveOk) {
    SetOpen(true);
  }
  return valveOk;
}

bool SimulationValve::Close() {
  bool valveOk = !CheckValveOpen();
  if (!valveOk) {
    SetOpen(false);
  }
  return valveOk;
}

void SimulationValve::SetOpen(bool open) {
  Command cmd;
  cmd.AddCmd("SetDoor").AddCmd(doorSideStr).AddCmd("Valve").AddCmd(valveNumberStr);
  cmd.AddArg(open ? "open" : "close");
  sim->DoCommand(&cmd);

  std::string cmdResult = cmd.GetResult();
  if (cmdResult != "ack") {
    throw std::runtime_error(SSTR("Unexpected result in " << __PRETTY_FUNCTION__ << "  (" << cmdResult << ")"));
  }
}

bool SimulationValve::CheckValveOpen() {
  Command cmd;
  cmd.AddCmd("GetDoor").AddCmd(doorSideStr).AddCmd("Valve").AddCmd(valveNumberStr);
  sim->DoCommand(&cmd);

  bool result(false);
  std::string cmdResult = cmd.GetResult();
  if (cmdResult == "open") {
    result = true;
  } else if (cmdResult == "closed") {
    result = false;
  } else {
    throw std::runtime_error(SSTR("Unexpected result in " << __PRETTY_FUNCTION__ << "  (" << cmdResult << ")"));
  }
  return result;
}
