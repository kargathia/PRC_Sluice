#include "SimulationFastLockSluiceDoor.h"

SimulationFastLockSluiceDoor::SimulationFastLockSluiceDoor(DoorSide::DoorSide type) : SimulationSluiceDoor(type) {}

bool SimulationFastLockSluiceDoor::Close() {
  SimulationSluiceDoor::Close();
  DoorState::DoorState state = GetState();
  if (state == DoorState::CLOSED) {
    Lock(true);
  }
  return (state == DoorState::LOCKED);
}
