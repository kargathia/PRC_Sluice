#include <SluiceSim.h>
#include <log.h>
#include "SimulationFastLockSluiceDoor.h"
#include "SimulationSluice.h"
#include "SimulationSteppedSluiceDoor.h"
#include "SluiceFactory.h"
#include "SluiceStates.h"

namespace {
const int NUM_VALVES = 3;
const char* ADDRESS = "localhost";

const char* PORT_KEY = "port";
const char* DOOR_CLASS_KEY = "doorClass";
}

std::shared_ptr<ISluice> SluiceFactory::GetSluice(const ConfigMap_t& config) {
  int simPort = std::stoi(config.at(PORT_KEY));

  std::shared_ptr<SimulationSluice> sluice = std::make_shared<SimulationSluice>();
  sluice->InitSim(ADDRESS, simPort);
  sluice->AddDoor(MakeDoor(config, DoorSide::LEFT));
  sluice->AddDoor(MakeDoor(config, DoorSide::RIGHT));
  sluice->BindActions();
  return sluice;
}

std::shared_ptr<SimulationSluiceDoor> SluiceFactory::MakeDoor(const ConfigMap_t& config,
                                                              DoorSide::DoorSide orientation) {
  std::shared_ptr<SimulationSluiceDoor> door;
  std::string doorClass = config.at(DOOR_CLASS_KEY);

  if (doorClass == "normal") {
    door = std::make_shared<SimulationSluiceDoor>(orientation);
  } else if (doorClass == "fastlock") {
    door = std::make_shared<SimulationFastLockSluiceDoor>(orientation);
  } else if (doorClass == "stepped") {
    door = std::make_shared<SimulationSteppedSluiceDoor>(orientation);
  } else {
    throw std::invalid_argument("Unknown door type: " + orientation);
  }

  int simPort = std::stoi(config.at(PORT_KEY));
  door->InitSim(ADDRESS, simPort);
  AddValves(config, door);
  AddLights(config, door);

  LOG(INFO) << "Created " << doorClass << " sluice door at port " << simPort;

  return door;
}

void SluiceFactory::AddValves(const ConfigMap_t& config, std::shared_ptr<SimulationSluiceDoor> door) {
  DoorSide::DoorSide side = door->GetSide();
  int simPort = std::stoi(config.at(PORT_KEY));

  for (int i(0); i < NUM_VALVES; ++i) {
    int id = (i + 1);
    std::shared_ptr<SimulationValve> valve = std::make_shared<SimulationValve>(id, side);
    valve->InitSim(ADDRESS, simPort);
    door->AddValve(valve);
  }
}

void SluiceFactory::AddLights(const ConfigMap_t& config, std::shared_ptr<SimulationSluiceDoor> door) {
  if (door->GetSide() == DoorSide::LEFT) {
    door->SetOuterLight(InitLight(config, 1));
    door->SetInnerLight(InitLight(config, 2));
  } else {
    door->SetInnerLight(InitLight(config, 3));
    door->SetOuterLight(InitLight(config, 4));
  }
}

std::shared_ptr<SimulationTrafficLight> SluiceFactory::InitLight(const ConfigMap_t& config, int idx) {
  int simPort = std::stoi(config.at(PORT_KEY));
  std::shared_ptr<SimulationTrafficLight> light = std::make_shared<SimulationTrafficLight>(idx);
  light->InitSim(ADDRESS, simPort);
  return light;
}
