#include <SSTR.h>
#include <memory>
#include "SimulationSluiceDoor.h"

SimulationSluiceDoor::SimulationSluiceDoor(DoorSide::DoorSide type) : doorSide(type) {
  doorSideText = DoorSide::ToString(type);
}

SimulationSluiceDoor::~SimulationSluiceDoor() {}

DoorState::DoorState SimulationSluiceDoor::GetState() {
  Command doorCommand;
  doorCommand.AddCmd("GetDoor").AddCmd(doorSideText);
  sim->DoCommand(&doorCommand);
  return DoorState::FromString(doorCommand.GetResult());
}

WaterLevel::WaterLevel SimulationSluiceDoor::GetWaterLevel() {
  Command cmd;
  cmd.AddCmd("GetWaterLevel");
  sim->DoCommand(&cmd);
  return WaterLevel::FromString(cmd.GetResult());
}

bool SimulationSluiceDoor::CheckClosed() {
  DoorState::DoorState state = GetState();
  return (state == DoorState::CLOSED || state == DoorState::LOCKED);
}

bool SimulationSluiceDoor::Open() {
  DoorState::DoorState state = GetState();
  Command cmd;

  if (!CloseValves()) {
    return false;
  }

  switch (state) {
    case DoorState::OPEN:
    case DoorState::OPENING:
      break;  // ignore
    case DoorState::LOCKED: {
      Lock(false);
      break;
    }
    case DoorState::CLOSING: {
      Stop();
      break;
    }
    case DoorState::CLOSED:
    case DoorState::STOPPED: {
      cmd.AddCmd("SetDoor").AddCmd(doorSideText).AddArg("open");
      sim->DoCommand(&cmd);
      break;
    }
    case DoorState::MOTOR_DAMAGE:
      throw std::runtime_error("Motor damaged");
      break;
    default:
      throw std::invalid_argument("Unknown door state");
  }

  return (state == DoorState::OPEN);
}

bool SimulationSluiceDoor::Close() {
  DoorState::DoorState state = GetState();
  Command cmd;

  if (!CloseValves()) {
    return false;
  }

  if (!outerLight->SetRed() || !innerLight->SetRed()) {
    return false;
  }

  switch (state) {
    case DoorState::LOCKED:
    case DoorState::CLOSING:
    case DoorState::CLOSED:
      break;  // ignore
    case DoorState::OPENING: {
      Stop();
      break;
    }
    case DoorState::OPEN:
    case DoorState::STOPPED: {
      cmd.AddCmd("SetDoor").AddCmd(doorSideText).AddArg("close");
      sim->DoCommand(&cmd);
      break;
    }
    case DoorState::MOTOR_DAMAGE: {
      throw std::runtime_error("Motor damaged");
      break;
    }
    default:
      throw std::invalid_argument("Unknown door state");
  }

  return (state == DoorState::LOCKED || state == DoorState::CLOSED);
}

bool SimulationSluiceDoor::Stop() {
  DoorState::DoorState state = GetState();
  bool isOk = (state == DoorState::LOCKED || state == DoorState::CLOSED || state == DoorState::STOPPED);
  if (!isOk) {
    Command doorCommand;
    doorCommand.AddCmd("SetDoor").AddCmd(doorSideText).AddArg("stop");
    sim->DoCommand(&doorCommand);
  }
  return isOk;
}

bool SimulationSluiceDoor::EmergencyStop() { return Stop() && CloseValves(); }

bool SimulationSluiceDoor::AllowEntry() {
  if (GetState() != DoorState::OPEN) {
    throw std::invalid_argument("Unable to allow entry while door is not open");
  }
  bool result = innerLight->SetRed() && outerLight->SetGreen();
  return result;
}

bool SimulationSluiceDoor::AllowExit() {
  if (GetState() != DoorState::OPEN) {
    throw std::invalid_argument("Unable to allow exit while door is not open");
  }
  bool result = outerLight->SetRed() && innerLight->SetGreen();
  return result;
}

bool SimulationSluiceDoor::Lower() {
  if (!CheckClosed()) {
    throw std::invalid_argument("Unable to lower water level while door is open");
  }

  WaterLevel::WaterLevel water = GetWaterLevel();
  switch (water) {
    case WaterLevel::LOW: {
      CloseValves();
      break;
    }
    case WaterLevel::BELOW_VALVE_2:
    case WaterLevel::ABOVE_VALVE_2:
    case WaterLevel::ABOVE_VALVE_3:
    case WaterLevel::HIGH: {
      valves[3]->Close();
      valves[2]->Close();
      valves[1]->Open();
      break;
    }
    default: {
      throw std::runtime_error(SSTR("Unexpected water level in " << __PRETTY_FUNCTION__));
      break;
    }
  }
  return (water == WaterLevel::LOW);
}

bool SimulationSluiceDoor::Raise() {
  if (!CheckClosed()) {
    throw std::invalid_argument("Unable to raise water level while door is open");
  }

  WaterLevel::WaterLevel water = GetWaterLevel();
  switch (water) {
    case WaterLevel::LOW:
    case WaterLevel::BELOW_VALVE_2: {
      valves[1]->Open();
      break;
    }
    case WaterLevel::ABOVE_VALVE_2: {
      valves[2]->Open();
      break;
    }
    case WaterLevel::ABOVE_VALVE_3: {
      valves[3]->Open();
      break;
    }
    case WaterLevel::HIGH: {
      CloseValves();
      break;
    }
    default: {
      throw std::runtime_error(SSTR("Unexpected water level in " << __PRETTY_FUNCTION__));
      break;
    }
  }
  return (water == WaterLevel::HIGH);
}

void SimulationSluiceDoor::Lock(bool locked) {
  Command cmd;
  cmd.AddCmd("SetDoorLock").AddCmd(doorSideText).AddArg(locked ? "on" : "off");
  sim->DoCommand(&cmd);
}

bool SimulationSluiceDoor::CloseValves() {
  bool result = true;
  for (auto valvePair : valves) {
    result = result && valvePair.second->Close();
  }
  return result;
}
