#include "SimulationSteppedSluiceDoor.h"

SimulationSteppedSluiceDoor::SimulationSteppedSluiceDoor(DoorSide::DoorSide type) : SimulationSluiceDoor(type) {}

bool SimulationSteppedSluiceDoor::Open() {
  DoorState::DoorState state = GetState();
  Command cmd;

  if (!CloseValves()) {
    return false;
  }

  switch (state) {
    case DoorState::OPEN:
      break;  // ignore
    case DoorState::LOCKED: {
      Lock(false);
      break;
    }
    case DoorState::CLOSING: {
      Stop();
      break;
    }
    case DoorState::OPENING:  // also repeat command if door is busy opening
    case DoorState::CLOSED:
    case DoorState::STOPPED: {
      cmd.AddCmd("SetDoor").AddCmd(doorSideText).AddArg("open");
      sim->DoCommand(&cmd);
      break;
    }
    case DoorState::MOTOR_DAMAGE:
      throw std::runtime_error("Motor damaged");
      break;
    default:
      throw std::invalid_argument("Unknown door state");
  }

  return (state == DoorState::OPEN);
}

bool SimulationSteppedSluiceDoor::Close() {
  DoorState::DoorState state = GetState();
  Command cmd;

  if (!CloseValves()) {
    return false;
  }

  if (!outerLight->SetRed() || !innerLight->SetRed()) {
    return false;
  }

  switch (state) {
    case DoorState::LOCKED:
    case DoorState::CLOSED:
      break;  // ignore
    case DoorState::OPENING: {
      Stop();
      break;
    }
    case DoorState::CLOSING:  // Also repeat command if door is busy closing
    case DoorState::OPEN:
    case DoorState::STOPPED: {
      cmd.AddCmd("SetDoor").AddCmd(doorSideText).AddArg("close");
      sim->DoCommand(&cmd);
      break;
    }
    case DoorState::MOTOR_DAMAGE: {
      throw std::runtime_error("Motor damaged");
      break;
    }
    default:
      throw std::invalid_argument("Unknown door state");
  }

  return (state == DoorState::LOCKED || state == DoorState::CLOSED);
}
