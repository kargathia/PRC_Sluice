#include <SSTR.h>
#include <memory>
#include "SimulationTrafficLight.h"

SimulationTrafficLight::SimulationTrafficLight(int nr) : lightNumber(nr), lightNumberStr(std::to_string(nr)) {}

bool SimulationTrafficLight::SetGreen() {
  bool greenOn = CheckLight(GREEN);
  bool redOff = !CheckLight(RED);

  if (!greenOn) {
    SetLight(GREEN, true);
  }

  if (!redOff) {
    SetLight(RED, false);
  }

  return redOff && greenOn;
}

bool SimulationTrafficLight::SetRed() {
  bool greenOff = !CheckLight(GREEN);
  bool redOn = CheckLight(RED);

  if (!greenOff) {
    SetLight(GREEN, false);
  }

  if (!redOn) {
    SetLight(RED, true);
  }

  return redOn && greenOff;
}

bool SimulationTrafficLight::CheckLight(const std::string& color) {
  Command cmd;
  cmd.AddCmd("GetTrafficLight").AddCmd(lightNumberStr).AddCmd(color);
  sim->DoCommand(&cmd);

  bool result(false);
  std::string cmdResult = cmd.GetResult();
  if (cmdResult == "on") {
    result = true;
  } else if (cmdResult == "off") {
    result = false;
  } else {
    throw std::runtime_error(SSTR("Unexpected result in " << __PRETTY_FUNCTION__ << "  (" << cmdResult << ")"));
  }
  return result;
}

void SimulationTrafficLight::SetLight(const std::string& color, bool on) {
  Command cmd;
  cmd.AddCmd("SetTrafficLight").AddCmd(lightNumberStr).AddCmd(color);
  cmd.AddArg(on ? "on" : "off");
  sim->DoCommand(&cmd);

  std::string cmdResult = cmd.GetResult();
  if (cmdResult != "ack") {
    throw std::runtime_error(SSTR("Unexpected result in " << __PRETTY_FUNCTION__ << "  (" << cmdResult << ")"));
  }
}
