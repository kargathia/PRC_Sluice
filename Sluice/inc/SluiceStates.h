#pragma once

#include <stdexcept>
#include <string>

namespace DoorSide {
enum DoorSide { LEFT, RIGHT };

inline std::string ToString(DoorSide type) {
  std::string retval;
  switch (type) {
    case LEFT: {
      retval = "Left";
      break;
    }
    case RIGHT: {
      retval = "Right";
      break;
    }
    default:
      throw std::invalid_argument("Unsupported door type");
  }
  return retval;
}
}  // namespace DoorSide

namespace SluiceState {
enum SluiceState { OPEN, CLOSED, EMERGENCY, ENTRY_ALLOWED_LEFT, EXIT_ALLOWED, ENTRY_ALLOWED };
}  // namespace SluiceState

namespace SluiceAction {
enum SluiceAction {
  STARTING,
  OPENING,
  CLOSING,
  EMERGENCY_STOPPING,
  EMERGENCY_RESUMING,
  LOWERING,
  RAISING,
  ALLOWING_EXIT,
  ALLOWING_ENTRY,
  IDLE
};
}  // namespace SluiceAction

namespace WaterLevel {
enum WaterLevel { LOW, BELOW_VALVE_2, ABOVE_VALVE_2, ABOVE_VALVE_3, HIGH };

inline WaterLevel FromString(const std::string& str) {
  WaterLevel retval = LOW;
  if (str == "low") {
    retval = WaterLevel::LOW;
  } else if (str == "belowValve2") {
    retval = WaterLevel::BELOW_VALVE_2;
  } else if (str == "aboveValve2") {
    retval = WaterLevel::ABOVE_VALVE_2;
  } else if (str == "aboveValve3") {
    retval = WaterLevel::ABOVE_VALVE_3;
  } else if (str == "high") {
    retval = WaterLevel::HIGH;
  } else {
    throw std::invalid_argument("unrecognized water level");
  }
  return retval;
}
}  // namespace WaterLevel

namespace DoorState {
enum DoorState { LOCKED, OPEN, CLOSED, CLOSING, OPENING, STOPPED, MOTOR_DAMAGE };

inline DoorState FromString(const std::string& str) {
  if (str == "doorLocked") {
    return LOCKED;
  } else if (str == "doorOpen") {
    return OPEN;
  } else if (str == "doorClosed") {
    return CLOSED;
  } else if (str == "doorClosing") {
    return CLOSING;
  } else if (str == "doorOpening") {
    return OPENING;
  } else if (str == "doorStopped") {
    return STOPPED;
  } else if (str == "motorDamage") {
    return MOTOR_DAMAGE;
  } else {
    std::string error = "Unknown DoorState: " + str;
    throw std::invalid_argument(error);
  }
}
}  // namespace DoorState
