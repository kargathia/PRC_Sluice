#pragma once

#include <SluiceSim.h>
#include <atomic>
#include <future>
#include <map>
#include <memory>
#include <string>
#include <thread>
#include "ISluice.h"
#include "SimCommander.h"
#include "SimulationSluiceDoor.h"
#include "SluiceStates.h"

class ISluiceDoor;

class SimulationSluice : public ISluice, public SimCommander {
 public:
  SimulationSluice();
  virtual ~SimulationSluice();

  void AddDoor(std::shared_ptr<SimulationSluiceDoor> door) { doors.emplace(door->GetSide(), door); }
  void SetUpperSide(DoorSide::DoorSide side) { upperSide = side; }
  void SetLowerSide(DoorSide::DoorSide side) { lowerSide = side; }
  void BindActions();

 public:  // ISluice
  void Start();
  void StartEmergency();
  void EndEmergency();
  void AllowEntry();
  void AllowExit();

 private:
  void Open();
  void Close();
  void Lower();
  void Raise();

 private:
  void Run();
  void StartAction(SluiceAction::SluiceAction action);
  void EndAction(SluiceAction::SluiceAction action);
  WaterLevel::WaterLevel GetWaterLevel();

 private:
  void ActionStarting();
  void ActionOpening();
  void ActionClosing();
  void ActionEmergencyStopping();
  void ActionEmergencyResuming();
  void ActionLowering();
  void ActionRaising();
  void ActionAllowingExit();
  void ActionAllowingEntry();

 private:  // states
  DoorSide::DoorSide activeDoor = DoorSide::LEFT;
  SluiceState::SluiceState sluiceState = SluiceState::CLOSED;
  WaterLevel::WaterLevel waterLevel = WaterLevel::LOW;
  SluiceAction::SluiceAction currentAction = SluiceAction::IDLE;
  SluiceAction::SluiceAction previousAction = SluiceAction::IDLE;

 private:
  std::map<DoorSide::DoorSide, std::shared_ptr<SimulationSluiceDoor>> doors;
  DoorSide::DoorSide lowerSide = DoorSide::LEFT;
  DoorSide::DoorSide upperSide = DoorSide::RIGHT;

 private:  // runner vars
  std::atomic_bool isRunning;
  std::thread runner;
  std::condition_variable actionCV;
  std::mutex actionMtx;
  std::map<SluiceAction::SluiceAction, std::function<void()>> actions;
};
