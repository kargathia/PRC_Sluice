#pragma once

#include <stdexcept>
#include <string>
#include "SimCommander.h"

class SimulationTrafficLight : public SimCommander {
 public:
  explicit SimulationTrafficLight(int nr);
  virtual ~SimulationTrafficLight() {}

  bool SetGreen();
  bool SetRed();

  int GetNumber() { return lightNumber; }

 private:
  void SetLight(const std::string& color, bool on);
  bool CheckLight(const std::string& color);

 private:
  const int lightNumber;
  const std::string lightNumberStr;
  const std::string RED = "Red";
  const std::string GREEN = "Green";
};
