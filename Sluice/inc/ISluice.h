#pragma once

class ISluice {
 public:
  virtual ~ISluice() {}

  virtual void Start() = 0;
  virtual void StartEmergency() = 0;
  virtual void EndEmergency() = 0;
  virtual void AllowEntry() = 0;
  virtual void AllowExit() = 0;
};
