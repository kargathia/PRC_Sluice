#pragma once

#include "SimulationSluiceDoor.h"

class SimulationSteppedSluiceDoor : public SimulationSluiceDoor {
 public:
  explicit SimulationSteppedSluiceDoor(DoorSide::DoorSide type);
  virtual ~SimulationSteppedSluiceDoor() {}

 public:  // overriding behavior
  virtual bool Open();
  virtual bool Close();
};
