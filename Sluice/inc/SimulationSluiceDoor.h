#pragma once

#include <SluiceSim.h>
#include <map>
#include <memory>
#include <string>
#include "SimCommander.h"
#include "SimulationTrafficLight.h"
#include "SimulationValve.h"
#include "SluiceStates.h"

class SimulationSluiceDoor : public SimCommander {
 public:
  explicit SimulationSluiceDoor(DoorSide::DoorSide side);
  virtual ~SimulationSluiceDoor();

  void SetInnerLight(std::shared_ptr<SimulationTrafficLight> light) { innerLight = light; }
  void SetOuterLight(std::shared_ptr<SimulationTrafficLight> light) { outerLight = light; }

  void AddValve(std::shared_ptr<SimulationValve> newValve) { valves.emplace(newValve->GetNumber(), newValve); }

 public:
  virtual bool Open();
  virtual bool Close();
  virtual bool Stop();
  virtual bool EmergencyStop();

  virtual bool AllowEntry();
  virtual bool AllowExit();

  virtual bool Lower();
  virtual bool Raise();

  virtual bool CheckClosed();
  WaterLevel::WaterLevel GetWaterLevel();
  DoorState::DoorState GetState();
  DoorSide::DoorSide GetSide() { return doorSide; }

 protected:
  virtual bool CloseValves();
  virtual void Lock(bool locked);

 protected:  // state
  DoorState::DoorState state = DoorState::LOCKED;
  DoorSide::DoorSide doorSide;
  std::string doorSideText;

 protected:
  std::shared_ptr<SimulationTrafficLight> innerLight;
  std::shared_ptr<SimulationTrafficLight> outerLight;
  std::map<int, std::shared_ptr<SimulationValve>> valves;
};
