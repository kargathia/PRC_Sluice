#pragma once

#include <string>
#include "SimCommander.h"
#include "SluiceStates.h"

class SimulationValve : public SimCommander {
 public:
  SimulationValve(int nr, DoorSide::DoorSide type);
  virtual ~SimulationValve() {}

 public:
  bool Open();
  bool Close();

  int GetNumber() { return valveNumber; }

 private:
  void SetOpen(bool open);
  bool CheckValveOpen();

 private:
  const int valveNumber;
  const DoorSide::DoorSide doorSide;

  const std::string doorSideStr;
  const std::string valveNumberStr;
};
