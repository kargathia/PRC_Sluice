#pragma once

#include "SimulationSluiceDoor.h"

class SimulationFastLockSluiceDoor : public SimulationSluiceDoor {
 public:
  explicit SimulationFastLockSluiceDoor(DoorSide::DoorSide type);
  virtual ~SimulationFastLockSluiceDoor() {}

  virtual bool Close();  // overriding SimulationSluiceDoor
};
