#pragma once

#include <map>
#include <memory>
#include <stdexcept>
#include <string>
#include "ISluice.h"

typedef std::map<std::string, std::string> ConfigMap_t;

class SimulationSluiceDoor;
class SimulationTrafficLight;

namespace DoorSide {
enum DoorSide;
}

class SluiceFactory {
 public:
  std::shared_ptr<ISluice> GetSluice(const ConfigMap_t& config);

 private:
  std::shared_ptr<SimulationSluiceDoor> MakeDoor(const ConfigMap_t& config, DoorSide::DoorSide orientation);
  void AddValves(const ConfigMap_t& config, std::shared_ptr<SimulationSluiceDoor> door);
  void AddLights(const ConfigMap_t& config, std::shared_ptr<SimulationSluiceDoor> door);
  std::shared_ptr<SimulationTrafficLight> InitLight(const ConfigMap_t& config, int idx);
};
