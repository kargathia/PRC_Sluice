#pragma once

#include <SluiceSim.h>
#include <memory>
#include <string>

class SimCommander {
 public:
  SimCommander() {}
  virtual ~SimCommander() {}

  void InitSim(const std::string& address, int port) { sim = std::make_shared<SluiceSim>(address, port); }

 protected:
  std::shared_ptr<SluiceSim> sim;
};
