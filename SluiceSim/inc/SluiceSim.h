#pragma once

#include <string>
#include "Command.h"

class SluiceSim {
 public:
  SluiceSim(const std::string& address, int port);
  virtual ~SluiceSim() {}

  void DoCommand(Command* cmd);

 private:
  const std::string simAddress;
  const int simPort;

 private:
  static const int ATTEMPT_TIMEOUT_MS = 1000;
  static const int MAX_ATTEMPTS = 10;
};
