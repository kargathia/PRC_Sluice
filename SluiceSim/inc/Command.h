#pragma once

#include <algorithm>
#include <iterator>
#include <sstream>
#include <string>
#include <vector>

class Command {
 public:
  Command() {}
  virtual ~Command() {}

  inline Command& AddCmd(const std::string& cmdPart) {
    cmd.push_back(cmdPart);
    return *this;
  }

  inline Command& AddArg(const std::string& arg) {
    args.push_back(arg);
    return *this;
  }

  inline void SetResult(const std::string& res) { result = res; }

  inline const std::string& GetResult() { return result; }

  inline std::string GetAction() {
    std::stringstream ss;
    std::copy(cmd.begin(), cmd.end(), std::ostream_iterator<std::string>(ss));

    if (!args.empty()) {
      ss << CMD_PARAM_DELIM;
      std::copy(args.begin(), args.end(), std::ostream_iterator<std::string>(ss));
    }

    ss << CMD_END;
    return ss.str();
  }

 private:
  std::vector<std::string> cmd;
  std::vector<std::string> args;
  std::string result;

 public:
  const std::string CMD_PARAM_DELIM = ":";
  const std::string CMD_END = ";";
};
