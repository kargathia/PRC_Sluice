#include <Connection.h>
#include <Message.h>
#include <log.h>
#include "SluiceSim.h"

SluiceSim::SluiceSim(const std::string& address, int port) : simAddress(address), simPort(port) {}

void SluiceSim::DoCommand(Command* cmd) {
  Message msg(cmd->GetAction());
  Connection conn;
  conn.SetMetaDataEnabled(false);
  conn.Connect(simAddress, simPort);
  conn.Send(cmd->GetAction());

  for (int i(0); i < MAX_ATTEMPTS; ++i) {
    conn.Poll(ATTEMPT_TIMEOUT_MS);
    std::string result = conn.Peek();
    if (result.find(cmd->CMD_END) != std::string::npos) {
      result.pop_back();  // remove control character
      cmd->SetResult(result);
      break;
    }
  }
}
