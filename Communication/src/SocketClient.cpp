#include <errno.h>
#include <fcntl.h>
#include <log.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <chrono>
#include <mutex>
#include <thread>
#include "SocketClient.h"
#include "SocketException.h"

SocketClient::SocketClient() { Configure(); }

SocketClient::SocketClient(int fDesc, const sockaddr_in& address) { Configure(address, fDesc); }

SocketClient::~SocketClient() { DepleteSendBuffer(2); }

AddressInfo SocketClient::GetPeerName() { return Socket::GetPeerName(remoteFDesc); }

bool SocketClient::Connect(const std::string& address, int port) {
  if (!CheckValid(GetFDesc())) {
    return false;
  }

  if (!SetBlocking(true)) {
    LOG(DEBUG) << "Unable to set blocking";
    return false;
  }

  int status = 0;
  struct addrinfo hints;
  struct addrinfo* serverAddress;

  // first, load up address structs with getaddrinfo():
  memset(&hints, 0, sizeof hints);
  hints.ai_family = AF_INET;
  hints.ai_socktype = SOCK_STREAM;

  // protocol should be represented as a const char*
  // We'll convert port to std::string and then to const char*
  status = ::getaddrinfo(address.c_str(), std::to_string(port).c_str(), &hints, &serverAddress);

  if (status < 0) {
    LOG(ERROR) << "Getting address info for " << address << ":" << port << " failed: " << GetErrString(errno);
    return false;
  }

  remoteFDesc = ::connect(GetFDesc(), serverAddress->ai_addr, serverAddress->ai_addrlen);
  status = remoteFDesc;

  if (status < 0) {
    LOG(ERROR) << "Connecting to " << address << ":" << port << " failed: " << GetErrString(errno);
    return false;
  }
  return status >= 0;
}

bool SocketClient::Send(const std::string& message) {
  if (!SetBlocking(true)) {
    return false;
  }
  return Socket::Send(GetFDesc(), message);
}

void SocketClient::DepleteSendBuffer(int attempts) {
  int fd = GetFDesc();
  for (int i = 0; i < attempts; ++i) {
    int outstanding;
    ::ioctl(fd, TIOCOUTQ, &outstanding);
    if (!outstanding) {
      break;
    }

    usleep(100);
  }
}
