#include <SSTR.h>
#include <errno.h>
#include <fcntl.h>
#include <log.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <mutex>   //NOLINT
#include <thread>  //NOLINT
#include "Socket.h"
#include "SocketException.h"

namespace {
const int RECEIVE_TIMEOUT_S = 0;
const int SEND_TIMEOUT_S = 0;

const int RECEIVE_TIMEOUT_US = 50 * 1000;
const int SEND_TIMEOUT_US = 50 * 1000;

const int ON = 1;
const int OFF = 0;

std::mutex SocketCreateMutex;
}  // namespace

std::string Socket::GetErrString(int err) { return SSTR(strerror(err) << "(" << err << ")"); }

Socket::Socket() : myFDesc(-1) { ::memset(&myAddress, 0, sizeof(myAddress)); }

Socket::~Socket() { Disconnect(); }

void Socket::Disconnect() {
  if (myFDesc >= 0) {
    int retVal = ::close(myFDesc);
    LOG_IF((retVal < 0), WARNING) << "Failed to disconnect " << myFDesc << ": " << GetErrString(errno);
  }
  myFDesc = -1;
}

bool Socket::CheckValid(int fDesc) { return (fcntl(fDesc, F_GETFL) != -1 || errno != EBADF); }

int Socket::Create() {
  std::lock_guard<std::mutex> lock(SocketCreateMutex);
  return ::socket(AF_INET, SOCK_STREAM, 0);
}

bool Socket::Configure() {
  sockaddr_in address;
  address.sin_family = AF_INET;
  address.sin_addr.s_addr = INADDR_ANY;
  address.sin_port = 0;

  return Configure(address);
}

bool Socket::Configure(const sockaddr_in &address) { return Configure(address, Create()); }

bool Socket::Configure(const sockaddr_in &address, int fDesc) {
  if (!CheckValid(fDesc)) {
    return false;
  }

  myFDesc = fDesc;
  myAddress.sin_family = address.sin_family;
  myAddress.sin_addr.s_addr = address.sin_addr.s_addr;
  myAddress.sin_port = address.sin_port;

  struct timeval tv;

  tv.tv_usec = RECEIVE_TIMEOUT_US;
  tv.tv_sec = RECEIVE_TIMEOUT_S;
  if (::setsockopt(myFDesc, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv)) < 0) {
    LOG(ERROR) << "Unable to set receive timeout: " << GetErrString(errno);
    return false;
  }

  tv.tv_usec = SEND_TIMEOUT_US;
  tv.tv_sec = SEND_TIMEOUT_S;
  if (::setsockopt(myFDesc, SOL_SOCKET, SO_SNDTIMEO, &tv, sizeof(tv)) < 0) {
    LOG(ERROR) << "Unable to set send timeout: " << GetErrString(errno);
    return false;
  }

  struct sigaction sa;
  sa.sa_handler = SIG_IGN;
  sa.sa_flags = 0;
  if (sigaction(SIGPIPE, &sa, 0) == -1) {
    LOG(ERROR) << "Unable to ignore SIGPIPE: " << GetErrString(errno);
    return false;
  }

  if (!SetBlocking(false)) {
    return false;
  }

  return true;
}

bool Socket::SetBlocking(bool blocking) {
  int optionVal = blocking ? OFF : ON;
  int resultCode = ioctl(myFDesc, (int)FIONBIO, (char *)&optionVal);

  if (resultCode < 0) {
    LOG(ERROR) << "Unable to set blocking: " << GetErrString(errno);
  }

  return resultCode >= 0;
}

AddressInfo Socket::GetPeerName(int fDesc) {
  socklen_t len;
  struct sockaddr_storage addr;
  char ipstr[INET6_ADDRSTRLEN];
  int port;

  AddressInfo info;

  len = sizeof addr;
  if (::getpeername(fDesc, (struct sockaddr *)&addr, &len) < 0) {
    info.valid = false;
    info.address = "Invalid address";
    info.port = -1;
    return info;
  }

  // deal with both IPv4 and IPv6:
  if (addr.ss_family == AF_INET) {
    struct sockaddr_in *s = (struct sockaddr_in *)&addr;
    port = ntohs(s->sin_port);
    inet_ntop(AF_INET, &s->sin_addr, ipstr, sizeof ipstr);
  } else {  // AF_INET6
    struct sockaddr_in6 *s = (struct sockaddr_in6 *)&addr;
    port = ntohs(s->sin6_port);
    inet_ntop(AF_INET6, &s->sin6_addr, ipstr, sizeof ipstr);
  }

  info.fDesc = fDesc;
  info.address = ipstr;
  info.port = port;
  info.valid = true;

  return info;
}

bool Socket::Read(int fDesc, std::stringstream *output) {
  int status = 0;
  char buffer[READ_BUFFER_SIZE];
  uint32_t maxRead = READ_BUFFER_SIZE - 1;
  uint32_t length = 0;
  size_t received = 0;

  if (IsMetaDataEnabled()) {
    char metaBuffer[META_DATA_LENGTH];
    status = ::recv(fDesc, metaBuffer, META_DATA_LENGTH, 0);
    memcpy(&length, metaBuffer, META_DATA_LENGTH);
  } else {
    status = 1;
  }

  while (status > 0) {
    // receive message size or buffer size - whichever is smaller
    uint32_t remaining = length - received;
    uint32_t wantedBytes = (remaining > maxRead || !IsMetaDataEnabled()) ? maxRead : remaining;

    // avoids buffer contamination by prev message blocks
    memset(&buffer, 0, sizeof(buffer));
    status = ::recv(fDesc, buffer, wantedBytes, 0);

    if (buffer[0] != 0) {
      *output << buffer;
      received += strlen(buffer);
    }
    if (IsMetaDataEnabled() && received >= length) {
      break;
    }
  }

  if (status < 0 && errno != EAGAIN) {
    LOG(ERROR) << "Receive error: " << GetErrString(errno);
  }

  return received > 0;
}

bool Socket::Send(int fDesc, const std::string &message) {
  if (message.empty()) {
    return false;
  }
  size_t length = message.size();
  size_t sent = 0;
  const char *data = message.c_str();
  int status = 0;

  if (IsMetaDataEnabled()) {
    uint32_t messageSize = length;  // hard set to 32 bit to remain portable
    status = ::send(fDesc, &messageSize, sizeof(messageSize), MSG_NOSIGNAL);
  } else {
    status = 1;
  }

  while (sent < length && status > 0) {
    status = ::send(fDesc, &data[sent], length - sent, MSG_NOSIGNAL | MSG_DONTWAIT);

    if (status < 0) {
      LOG(DEBUG) << "send() error: " << GetErrString(errno);
      break;
    }
    sent += status;
  }

  return (sent == length);
}
