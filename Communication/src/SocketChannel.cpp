#include <log.h>
#include <stringsplit.h>
#include <unistd.h>
#include <iostream>
#include <memory>
#include <thread>
#include "IMessageListener.h"
#include "Message.h"
#include "SocketClient.h"
#include "SocketException.h"
#include "SocketServer.h"
#include "SocketVolatile.h"

#include "SocketChannel.h"

namespace {
const int POLL_TIMEOUT_MS = 200;
}

SocketChannel::SocketChannel(int localPort) : myPort(localPort), isRunning(false) {}

SocketChannel::~SocketChannel() {
  isRunning = false;
  myMessageCV.notify_all();

  if (myServerThread.joinable()) {
    myServerThread.join();
  }
  if (myHandlerThread.joinable()) {
    myHandlerThread.join();
  }
}

void SocketChannel::SetPort(int port) {
  if (isRunning) {
    throw std::runtime_error("Port cannot be changed while channel is running");
  }
  myPort = port;
}

void SocketChannel::SetMetaDataEnabled(bool enabled) {
  if (isRunning) {
    throw std::runtime_error("Unable to change meta data usage while running");
  }
  metaDataEnabled = enabled;
}

void SocketChannel::Start() {
  if (isRunning) {
    throw std::runtime_error("Channel is already running");
  }
  if (myPort < 0) {
    throw std::invalid_argument("Invalid port");
  }
  isRunning = true;
  myServerThread = std::thread(&SocketChannel::RunServer, this);
  myHandlerThread = std::thread(&SocketChannel::RunClient, this);
  std::this_thread::sleep_for(std::chrono::milliseconds(100));  // server needs time to bind
}

void SocketChannel::RunServer() {
  SocketServer server;
  server.SetMetaDataEnabled(metaDataEnabled);
  using std::placeholders::_1;
  using std::placeholders::_2;
  server.SetMessageHandler(std::bind(&SocketChannel::HandleMessage, this, _1, _2));
  bool boundOk = server.Bind(myPort);
  while (boundOk && isRunning) {
    server.Poll(POLL_TIMEOUT_MS);
  }
}

void SocketChannel::RunClient() {
  try {
    while (isRunning) {
      NotifyListeners();
    }
  } catch (std::exception &ex) {
    LOG(ERROR) << "SocketChannel client thread terminated with error: " << ex.what();
  }
}

bool SocketChannel::WaitMessage(Message *message) {
  std::unique_lock<std::mutex> lock(myMessageMtx);
  if (isRunning && myMessages.empty()) {
    myMessageCV.wait(lock, [this] { return !isRunning || !myMessages.empty(); });
  }

  if (isRunning && !myMessages.empty()) {
    *message = myMessages[0];
    myMessages.erase(myMessages.begin());
    return true;
  }
  return false;
}

void SocketChannel::NotifyListeners() {
  Message message("invalid");
  bool messageReceived = WaitMessage(&message);

  if (isRunning && !myQueuedListeners.empty()) {
    // We want to avoid modifying the listeners while they are being notified
    // But it is perfectly legal for functions called by OnMessage() to add a
    // listener
    // This way deadlocks are avoided, and the channel clearly guarantees that
    // the
    // new listener will not be called for the current message
    std::unique_lock<std::mutex> lock(myListenerQueueMtx);
    myListeners.insert(myListeners.end(), myQueuedListeners.begin(), myQueuedListeners.end());
    myQueuedListeners.clear();
  }

  if (!isRunning || !messageReceived) {
    return;
  }

  std::vector<size_t> erasedElements;

  for (auto rit = myListeners.rbegin(); rit != myListeners.rend(); ++rit) {
    // Because we are using weak pointers, we need to check if they expired
    // before we used them
    if (std::shared_ptr<IMessageListener> listenerPtr = (*rit).lock()) {
      try {
        MessageReply reply;
        listenerPtr->OnMessage(message, &reply);
        if (HandleReply(message, &reply)) {
          break;
        }
      } catch (std::exception &ex) {
        LOG(ERROR) << "Exception caught in " << __func__ << ": " << ex.what();
      }
    } else {
      erasedElements.push_back(std::distance(begin(myListeners), rit.base()) - 1);
    }
  }

  for (auto idx : erasedElements) {
    myListeners.erase(myListeners.begin() + idx);
  }
}

bool SocketChannel::HandleReply(const Message &message, MessageReply *reply) {
  if (!reply->reply.empty()) {
    SocketVolatile sender;
    sender.SetMetaDataEnabled(metaDataEnabled);
    sender.Send(message.GetSenderAddress().fDesc, reply->reply);
  }

  return reply->handled;
}

void SocketChannel::AddListener(std::weak_ptr<IMessageListener> listener) {
  std::unique_lock<std::mutex> lock(myListenerQueueMtx);
  myQueuedListeners.push_back(listener);
}

void SocketChannel::HandleMessage(int fDesc, std::string message) {
  {  // We want to notify outside our lock
    std::unique_lock<std::mutex> lock(myMessageMtx);
    myMessages.push_back(Message(message, Socket::GetPeerName(fDesc)));
  }
  myMessageCV.notify_one();
}

void SocketChannel::Send(const Message &message, const std::string &destination, int destinationPort) {
  if (message.GetContent().empty()) {
    throw std::invalid_argument("Message content was empty");
  }

  Connection conn;
  conn.SetMetaDataEnabled(metaDataEnabled);
  conn.Connect(destination, destinationPort);
  conn.Send(message.GetContent());
}

std::shared_ptr<Connection> SocketChannel::OpenConnection(const std::string &destination, int port) {
  std::shared_ptr<Connection> conn = std::make_shared<Connection>();
  conn->SetMetaDataEnabled(metaDataEnabled);
  conn->Connect(destination, port);
  return conn;
}
