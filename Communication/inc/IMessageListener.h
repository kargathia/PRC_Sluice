#pragma once

#include <string>
#include "Message.h"

struct MessageReply {
  bool handled = false;
  std::string reply;
};

class IMessageListener {
 public:
  virtual void OnMessage(const Message& message, MessageReply* reply) = 0;
  virtual void OnError(const std::string& error) = 0;
  virtual void OnConnect() = 0;
  virtual void OnDisconnect() = 0;
};
