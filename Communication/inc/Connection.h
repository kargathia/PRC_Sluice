#pragma once

#include <sstream>
#include <string>
#include "SocketClient.h"
#include "SocketException.h"

class Connection : public SocketClient {
 public:
  Connection();
  virtual ~Connection();

  bool Send(const std::string& message);
  bool Connect(const std::string& address, int port);
  void Disconnect() { Socket::Disconnect(); }

  bool Poll(int timeoutMs);
  std::string Peek();
  std::string Get();

 private:
  struct pollfd polledFD;
  std::stringstream cache;
};
