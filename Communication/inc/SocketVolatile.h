#pragma once

#include <string>
#include "Socket.h"

/*
Small wrapper for when we want to send to a file descriptor without owning it.
Creating a SocketClient/Socket for a given file descriptor would close it in the destructor.
*/
class SocketVolatile : public Socket {
 public:
  SocketVolatile() {}
  virtual ~SocketVolatile() {}

  bool Read(int fDesc, std::stringstream* output) { return Socket::Read(fDesc, output); }
  bool Send(int fDesc, const std::string& message) { return Socket::Send(fDesc, message); }
};
