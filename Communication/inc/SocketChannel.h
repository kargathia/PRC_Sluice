#pragma once

#include <atomic>
#include <condition_variable>
#include <future>
#include <memory>
#include <mutex>
#include <string>
#include <thread>
#include <vector>
#include "Connection.h"
#include "SocketException.h"
#include "SocketServer.h"

class SocketChannel {
 public:
  explicit SocketChannel(int localPort = -1);
  virtual ~SocketChannel();

 public:
  /*
  Starts listening on local port for incoming connections and messages.
  If you only want to use SocketChannel to send messages, you do not need to call this.
  */
  void Start();

  /*
  Adds a Listener to the channel. If the channel receives a message, its OnMessage() function will be called.
  OnMessage() will only be called by a single thread per channel.
  */
  void AddListener(std::weak_ptr<IMessageListener> listener);

  /*
  Sends a message to the destination address/port, and closes the connection immediately.
  */
  void Send(const Message& message, const std::string& destination, int destinationPort);

  /*
  Opens a connection to address/port, and returns the initialized Connection object.
  Does not send any messages yet.
  */
  std::shared_ptr<Connection> OpenConnection(const std::string& destination, int port);

  /*
  Sets port at which it will listen when started.
  Throws std::runtime_error if channel is currently started.
  */
  void SetPort(int port);

  /*
  Toggles use of meta data for server and client sockets.
  Default is true
  */
  void SetMetaDataEnabled(bool enabled);

  /*
  Returns local port, at which it will be listening when Start() is called.
  */
  int GetPort() const { return myPort; }

 private:
  void RunServer();
  void RunClient();
  void NotifyListeners();
  bool HandleReply(const Message& message, MessageReply* reply);
  void HandleMessage(int fDesc, std::string message);
  bool WaitMessage(Message* message);

 private:
  int myPort;
  bool metaDataEnabled = true;

  std::mutex myListenerQueueMtx;
  std::vector<std::weak_ptr<IMessageListener>> myQueuedListeners;
  std::vector<std::weak_ptr<IMessageListener>> myListeners;

  std::vector<Message> myMessages;
  std::condition_variable myMessageCV;
  std::mutex myMessageMtx;

  std::thread myServerThread;
  std::thread myHandlerThread;
  std::atomic_bool isRunning;

 private:  // disable copy constructor
  SocketChannel(SocketChannel&);
};
