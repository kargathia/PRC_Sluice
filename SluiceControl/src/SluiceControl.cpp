#include <FileConfigReader.h>
#include <ISluice.h>
#include <SSTR.h>
#include <SimulationSluice.h>
#include <SluiceFactory.h>
#include <log.h>
#include <signal.h>
#include <string.h>
#include <iostream>
#include <memory>
#include <vector>

INITIALIZE_EASYLOGGINGPP

namespace {
const char* START_CMD = "start";
const char* ALLOW_ENTRY_CMD = "allow_entry";
const char* ALLOW_EXIT_CMD = "allow_exit";
const char* EMERGENCY_ON_CMD = "emergency_on";
const char* EMERGENCY_OFF_CMD = "emergency_off";
const char* EXIT_CMD = "exit";
const char* HELP_CMD = "help";

std::shared_ptr<ISluice> sluice;
}  // namespace

void SigCatcher(int signum, siginfo_t* info, void* ptr) {
  LOG(INFO) << "caught " << strsignal(signum) << ", shutting down...";
  sluice.reset();
  exit(0);
}

void ShowCommands() {
  // First concat commands in one string to avoid it being split by log messages
  std::cout << SSTR("Available commands:" << std::endl
                                          << START_CMD << std::endl
                                          << ALLOW_ENTRY_CMD << std::endl
                                          << ALLOW_EXIT_CMD << std::endl
                                          << EMERGENCY_ON_CMD << std::endl
                                          << EMERGENCY_OFF_CMD << std::endl
                                          << EXIT_CMD << std::endl
                                          << HELP_CMD << std::endl);
}

int main(int argc, char** argv) {
  el::Configurations conf("logger.conf");
  el::Loggers::reconfigureLogger("default", conf);

  // ensures SIGTERM and SIGINT are caught and handled properly
  struct sigaction action;
  memset(&action, 0, sizeof(struct sigaction));
  action.sa_sigaction = SigCatcher;
  action.sa_flags = SA_SIGINFO;
  sigaction(SIGTERM, &action, NULL);
  sigaction(SIGINT, &action, NULL);

  try {
    std::string fileInput;

    if (argc > 1) {
      fileInput = argv[1];
      LOG(INFO) << "using configuration file " << fileInput;
    } else {
      std::cout << "Configuration file?" << std::endl;
      std::getline(std::cin, fileInput);
    }

    FileConfigReader reader;
    auto config = reader.Read(fileInput);

    SluiceFactory factory;
    sluice = factory.GetSluice(config);
  } catch (std::exception& ex) {
    LOG(ERROR) << "Error creating sluice: " << ex.what();
    exit(1);
  }

  ShowCommands();
  std::string menuInput;
  while (true) {
    std::getline(std::cin, menuInput);

    try {
      if (menuInput == START_CMD) {
        sluice->Start();
      } else if (menuInput == ALLOW_ENTRY_CMD) {
        sluice->AllowEntry();
      } else if (menuInput == ALLOW_EXIT_CMD) {
        sluice->AllowExit();
      } else if (menuInput == EMERGENCY_ON_CMD) {
        sluice->StartEmergency();
      } else if (menuInput == EMERGENCY_OFF_CMD) {
        sluice->EndEmergency();
      } else if (menuInput == EXIT_CMD) {
        break;
      } else if (menuInput == HELP_CMD) {
        ShowCommands();
      } else {
        std::cout << "Unknown command: " << menuInput << std::endl;
        ShowCommands();
      }
    } catch (std::exception& ex) {
      LOG(ERROR) << "Could not submit action: " << ex.what();
    }
  }

  sluice.reset();
  return 0;
}
