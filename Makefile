MODULES := Utils \
           Communication \
           SluiceSim \
           Sluice \
           DataManager
TEST    := $(MODULES) Test
RUN     := $(MODULES) SluiceControl
TESTER  := $(MODULES) SluiceTester
ALL     := $(MODULES) Test SluiceControl SluiceTester

.PHONY: $(ALL) all

all: $(ALL)

clean: TARGET=clean
clean: $(ALL)

cleaner: TARGET=cleaner
cleaner: $(ALL)

run: all
	@cd SluiceControl/bin && ./SluiceControl

test: all
	@cd Test/bin && ./Test

tester: all
	@cd SluiceTester/bin && ./SluiceTester

$(ALL):
	@echo "Making ***$@***"
	@cd $@ && make --no-print-directory $(TARGET) CXXTYPE=${CXXTYPE}
