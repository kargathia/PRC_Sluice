#!/usr/bin/env python

'''
    Simple socket server using threads
'''
 
import socket
import sys
import time
import signal

HOST = ''   # Symbolic name, meaning all available interfaces
PORT = int(sys.argv[1]) # Arbitrary non-privileged port
SOCKET = None

class GracefulKiller:
    def __init__(self):
        signal.signal(signal.SIGINT, self.exit_gracefully)
        signal.signal(signal.SIGTERM, self.exit_gracefully)

    def exit_gracefully(self, signum, frame):
        try:
            print("terminating...")
            SOCKET.close()
        except Exception:
                pass

SOCKET = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
#SOCKET.settimeout(1)
#Bind socket to local host and port
try:
    SOCKET.bind((HOST, PORT))
except socket.error as msg:
    print('Bind failed. Error Code : ' + str(msg[0]) + ' Message ' + msg[1])
    sys.exit()
     
 
#Start listening on socket
SOCKET.listen(10)
print('echo server now listening')
 
#wait to accept a connection - blocking call
conn, addr = SOCKET.accept()
print('Connected with ' + addr[0] + ':' + str(addr[1]))

while True:
    data = conn.recv(1024)
    if data:
        if data == "exit":
            break
        print("echoing: " + data)
        conn.send(data)



print("closing...")
SOCKET.close()