#include <Connection.h>
#include <SSTR.h>
#include <SocketChannel.h>
#include <log.h>
#include <stdexcept>
#include "MessageListenerImpl.h"
#include "gtest/gtest.h"

class ConnectionTest : public ::testing::Test {
 protected:
  Connection conn;
  SocketChannel channel;
  std::shared_ptr<MessageListenerImpl> listener;
  static const int POLL_TIMEOUT_MS = 1000;

  virtual void SetUp() {
    static int serverPort(7500);
    serverPort++;

    listener = std::make_shared<MessageListenerImpl>();
    listener->SetEchoServer(true);
    channel.SetPort(serverPort);
    channel.AddListener(listener);
    channel.Start();

    conn.Connect("localhost", serverPort);
  }

  virtual void TearDown() {}
};

TEST_F(ConnectionTest, Test_Echo) {
  conn.Send("hello");

  ASSERT_TRUE(conn.Poll(POLL_TIMEOUT_MS));
  ASSERT_EQ(conn.Peek(), "hello");
  ASSERT_EQ(conn.Get(), "hello");

  // We already got the message, so shouldn't be anything left
  ASSERT_FALSE(conn.Poll(POLL_TIMEOUT_MS));
  ASSERT_EQ(conn.Peek(), "");
  ASSERT_EQ(conn.Get(), "");
}

TEST_F(ConnectionTest, Test_Chaining) {
  conn.Send("ha");
  conn.Send("lf");

  ASSERT_TRUE(conn.Poll(POLL_TIMEOUT_MS));
  ASSERT_TRUE(conn.Poll(POLL_TIMEOUT_MS));
  ASSERT_EQ(conn.Get(), "half");
}
