#include <FileConfigReader.h>
#include <stdexcept>
#include <string>
#include "gtest/gtest.h"

class FileConfigReaderTest : public ::testing::Test {
 public:
  FileConfigReaderTest() {
    // initialization code here
  }

  void SetUp() {
    // code here will execute just before the test ensues
  }

  void TearDown() {
    // code here will be called just after the test completes
    // ok to through exceptions from here if need be
  }

  ~FileConfigReaderTest() {
    // cleanup any pending stuff, but no exceptions allowed
  }

  // put in any custom data members that you need
};

TEST_F(FileConfigReaderTest, normalcase) {
  FileConfigReader reader;
  std::map<std::string, std::string> config = reader.Read("testconfig.txt");
  ASSERT_EQ("9999", config["poort"]);
  ASSERT_EQ("normal", config["doortype"]);
  ASSERT_EQ("normal", config["motortype"]);
}

TEST_F(FileConfigReaderTest, filenotfound) {
  FileConfigReader reader;
  std::map<std::string, std::string> config;
  ASSERT_ANY_THROW(config = reader.Read("notvalueadress.txt"););
}

TEST_F(FileConfigReaderTest, testwrongdata) {
  FileConfigReader reader;
  std::map<std::string, std::string> config;
  ASSERT_ANY_THROW(config = reader.Read("testwrong.txt"););
}
