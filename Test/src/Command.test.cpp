#include <Command.h>
#include <SSTR.h>
#include <log.h>
#include <stdexcept>
#include "gtest/gtest.h"

TEST(CommandTest, Test_Command) {
  Command cmd;
  cmd.AddCmd("first").AddCmd("second").AddCmd("third");
  ASSERT_EQ(cmd.GetAction(), "firstsecondthird;");
}

TEST(CommandTest, Test_Args) {
  Command cmd;
  cmd.AddCmd("command").AddArg("arg").AddArg("ument");
  ASSERT_EQ(cmd.GetAction(), "command:argument;");
}
