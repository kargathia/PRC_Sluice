#include <SSTR.h>
#include <SimulationValve.h>
#include <SocketChannel.h>
#include <log.h>
#include "MessageListenerImpl.h"
#include "gtest/gtest.h"

class SimulationValveTest : public ::testing::Test {
 public:
  const int POLL_TIMEOUT_MS = 1000;
  const int INDEX = 2;

 protected:
  std::shared_ptr<SimulationValve> valve;
  std::shared_ptr<MessageListenerImpl> listener;
  SocketChannel server;

  virtual void SetUp() {
    static int serverPort(7600);
    serverPort++;

    valve = std::make_shared<SimulationValve>(INDEX, DoorSide::LEFT);
    listener = std::make_shared<MessageListenerImpl>();

    server.SetPort(serverPort);
    server.SetMetaDataEnabled(false);
    server.AddListener(listener);
    server.Start();

    valve->InitSim("localhost", serverPort);
  }

  virtual void TearDown() {}
};

TEST_F(SimulationValveTest, Test_Open) {
  listener->QueueReply("closed;");
  listener->QueueReply("ack;");
  ASSERT_FALSE(valve->Open());

  listener->QueueReply("open;");
  ASSERT_TRUE(valve->Open());
}

TEST_F(SimulationValveTest, Test_Close) {
  listener->QueueReply("open;");
  listener->QueueReply("ack;");
  ASSERT_FALSE(valve->Close());

  listener->QueueReply("closed;");
  ASSERT_TRUE(valve->Close());
}
