#include <SSTR.h>
#include <SimulationTrafficLight.h>
#include <SocketChannel.h>
#include <log.h>
#include "MessageListenerImpl.h"
#include "gtest/gtest.h"

class SimulationTrafficLightTest : public ::testing::Test {
 public:
  const int POLL_TIMEOUT_MS = 1000;
  const int LIGHT_INDEX = 2;

 protected:
  std::shared_ptr<SimulationTrafficLight> light;
  std::shared_ptr<MessageListenerImpl> listener;
  SocketChannel server;

  virtual void SetUp() {
    static int serverPort(7600);
    serverPort++;

    light = std::make_shared<SimulationTrafficLight>(LIGHT_INDEX);
    listener = std::make_shared<MessageListenerImpl>();

    server.SetPort(serverPort);
    server.SetMetaDataEnabled(false);
    server.AddListener(listener);
    server.Start();

    light->InitSim("localhost", serverPort);
  }

  virtual void TearDown() {}
};

TEST_F(SimulationTrafficLightTest, Test_Green) {
  listener->QueueReply("off;");
  listener->QueueReply("on;");
  listener->QueueReply("ack;");
  listener->QueueReply("ack;");
  ASSERT_FALSE(light->SetGreen());

  listener->QueueReply("on;");
  listener->QueueReply("off;");
  ASSERT_TRUE(light->SetGreen());
}

TEST_F(SimulationTrafficLightTest, Test_Red) {
  listener->QueueReply("on;");
  listener->QueueReply("off;");
  listener->QueueReply("ack;");
  listener->QueueReply("ack;");
  ASSERT_FALSE(light->SetRed());

  listener->QueueReply("off;");
  listener->QueueReply("on;");
  ASSERT_TRUE(light->SetRed());
}
