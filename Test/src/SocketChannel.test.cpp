#include <SocketChannel.h>
#include <SocketException.h>
#include <log.h>
#include <future>
#include "MessageListenerImpl.h"
#include "gtest/gtest.h"

namespace {
int LOCAL_PORT = 6000;   // Will be incremented for each test
int REMOTE_PORT = 6500;  // Will be incremented for each test

const int TIMEOUT_MS = 1000;
const char* ADDRESS = "localhost";
}

void Increment() {
  LOCAL_PORT++;
  REMOTE_PORT++;
}

TEST(SocketChannelTest, Test_Init) {
  Increment();
  SocketChannel channel(LOCAL_PORT);
  channel.Start();

  // Disabled until #36 (Implement replacement SocketChannel promise) is fixed
  // SocketChannel duplicate(LOCAL_PORT);
  // ASSERT_THROW(duplicate.Start(), SocketException);
}

TEST(SocketChannelTest, Test_Send) {
  Increment();
  SocketChannel local(LOCAL_PORT);
  local.Start();

  std::shared_ptr<MessageListenerImpl> listener = std::make_shared<MessageListenerImpl>();

  SocketChannel remote(REMOTE_PORT);
  remote.AddListener(listener);
  remote.Start();

  std::string sentMessage = "Testing 1 2 3";
  local.Send(Message(sentMessage), ADDRESS, REMOTE_PORT);
  std::string secondMessage = "Testing 5 6";
  local.Send(Message(secondMessage), ADDRESS, REMOTE_PORT);

  ASSERT_EQ(sentMessage, listener->ExpectMessage(TIMEOUT_MS).GetContent());
  ASSERT_EQ(secondMessage, listener->ExpectMessage(TIMEOUT_MS).GetContent());

  std::string largeString(10000, 'a');
  local.Send(Message(largeString), ADDRESS, REMOTE_PORT);
  Message largeMessage = listener->ExpectMessage(TIMEOUT_MS);
  ASSERT_EQ(largeString, largeMessage.GetContent());

  ASSERT_EQ(largeMessage.GetSenderAddress().address, "127.0.0.1");
  ASSERT_TRUE(largeMessage.GetSenderAddress().valid);

  ASSERT_THROW(local.Send(Message(""), ADDRESS, REMOTE_PORT), std::invalid_argument);
}

TEST(SocketChannelTest, Test_Connection) {
  Increment();
  SocketChannel remote(REMOTE_PORT);
  std::shared_ptr<MessageListenerImpl> listener = std::make_shared<MessageListenerImpl>();
  remote.AddListener(listener);
  remote.Start();

  SocketChannel local(LOCAL_PORT);
  std::shared_ptr<Connection> conn = local.OpenConnection("localhost", REMOTE_PORT);
  std::string firstHalf = "Row, row, row your boat, ";
  std::string secondHalf = "gently down the stream";
  conn->Send(firstHalf);
  conn->Send(secondHalf);

  std::string reply = listener->ExpectMessage(TIMEOUT_MS).GetContent();
  ASSERT_EQ(reply, firstHalf);
  reply = listener->ExpectMessage(TIMEOUT_MS).GetContent();
  ASSERT_EQ(reply, secondHalf);
}

TEST(SocketChannelTest, Test_Reply) {
  Increment();
  SocketChannel remote(REMOTE_PORT);
  std::shared_ptr<MessageListenerImpl> listener = std::make_shared<MessageListenerImpl>();
  remote.AddListener(listener);
  remote.Start();

  std::string reply = "pong";
  listener->QueueReply(reply);

  Connection conn;
  conn.Connect("localhost", REMOTE_PORT);
  conn.Send("ping");
  ASSERT_TRUE(conn.Poll(TIMEOUT_MS));
  ASSERT_EQ(conn.Get(), reply);
}
