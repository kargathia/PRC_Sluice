# Included by component make files. Generic rules

# Example declaration of variables
# #Compiler and Linker
# CC          := ${CXXTYPE}g++
# LNK         := ${CXXTYPE}ld

# #The Target Binary Program
# TARGET      := Processing
# TARTYPE     := static

# #The Directories, Source, Includes, Objects, Binary and Resources
# SRCDIR      := src
# INCDIR      := inc
# BUILDDIR    := obj
# TARGETDIR   := bin
# RESDIR      := res
# SRCEXT      := cpp
# DEPEXT      := d
# OBJEXT      := o

# #Flags, Libraries and Includes
# CFLAGS      += -c -Wall -std=c++11 -Werror
# LDFLAGS     += 
# INC         := -I$(INCDIR) -I/usr/local/include -I../zxing-cpp/cli/src/
# INCDEP      := -I$(INCDIR) 
# MODDEP      := zxing-client Utils

#---------------------------------------------------------------------------------
#DO NOT EDIT BELOW THIS LINE
#---------------------------------------------------------------------------------
SOURCES     := $(shell find $(SRCDIR) -type f -name *.$(SRCEXT))
OBJECTS     := $(patsubst $(SRCDIR)/%,$(BUILDDIR)/%,$(SOURCES:.$(SRCEXT)=.$(OBJEXT)))
LINKED      := $(foreach mod,$(MODDEP),../$(mod)/$(TARGETDIR)/$(mod).$(OBJEXT))
INC         += $(foreach mod,$(MODDEP),-I$(PWD)/../$(mod)/$(INCDIR))

CFLAGS      += -c -g -Wall -std=c++11 -Werror -DELPP_THREAD_SAFE -DELPP_FORCE_USE_STD_THREAD
LDFLAGS     += -pthread

EXETARGET   := $(TARGETDIR)/$(TARGET)
STATTARGET  := $(TARGETDIR)/$(TARGET).$(OBJEXT)

#Default Make
all: $(TARTYPE)

#Static library
static: directories $(STATTARGET)

#executable
exe: resources $(EXETARGET)

#Remake
remake: cleaner all

#Copy contents of Resources Directory (if exists) to Target Directory
resources: directories
		@[ ! -e $(RESDIR) ] || cp -rf $(RESDIR)/* $(TARGETDIR)/

#Make the output directories
directories:
		@mkdir -p $(TARGETDIR)
		@mkdir -p $(BUILDDIR)

#Clean only Objects
clean:
		@$(RM) -rf $(BUILDDIR)

#Full Clean, Objects and Binaries
cleaner: clean
		@$(RM) -rf $(TARGETDIR)

$(EXETARGET): $(OBJECTS)
		@echo "  $(CC) >> '$(EXETARGET)'"
		@$(CC) -o $(EXETARGET) $^ $(LINKED) $(LDFLAGS) 

$(STATTARGET): $(OBJECTS)
		@echo "  $(LNK) >> '$(STATTARGET)'"
		@$(LNK)  -r -o $(STATTARGET) $^

#Compile
$(BUILDDIR)/%.$(OBJEXT): $(SRCDIR)/%.$(SRCEXT)
		@mkdir -p $(dir $@)
		@echo "    $(CC) >> $@"
		@$(CC) $(CFLAGS) $(INC) -c -o $(PWD)/$@ $(PWD)/$<

#Non-File Targets
.PHONY: remake clean cleaner resources directories $(EXETARGET) $(STATTARGET)
