#include <Connection.h>
#include <log.h>
#include <iostream>
#include <memory>
#include <string>
#include <thread>

INITIALIZE_EASYLOGGINGPP

int main(int argc, char** argv) {
  el::Configurations conf("logger.conf");
  el::Loggers::reconfigureLogger("default", conf);

  int port;
  if (argc > 1) {
    port = std::stoi(argv[1]);
  } else {
    std::string input;
    std::cout << "port: ";
    std::getline(std::cin, input);
    port = std::stoi(input);
  }

  LOG(INFO) << "Port: " << port;

  std::shared_ptr<Connection> conn = std::make_shared<Connection>();
  conn->SetMetaDataEnabled(false);
  conn->Connect("localhost", port);

  std::thread poller([conn]() {
    try {
      while (true) {
        if (conn->Poll(1000)) {
          LOG(INFO) << "Received: " << conn->Get();
        }
      }
    } catch (std::exception& ex) {
      LOG(ERROR) << "Poller exited with error: " << ex.what();
    }
  });

  std::cout << "Send your commands: type 'exit' to exit" << std::endl;
  std::string input;
  while (true) {
    std::getline(std::cin, input);
    if (input == "exit") {
      break;
    } else {
      conn->Send(input);
    }
  }

  conn->Disconnect();
  poller.join();
}
