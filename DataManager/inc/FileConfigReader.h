#pragma once

#include <string>
#include <map>

#include "IConfigReader.h"

class FileConfigReader : public IConfigReader {
 public:
  std::map<std::string, std::string> Read(std::string address);
};
