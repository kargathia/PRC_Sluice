#pragma once

#include <string>
#include <map>

class IConfigReader {
 public:
  virtual std::map<std::string, std::string> Read(std::string address) = 0;
};
