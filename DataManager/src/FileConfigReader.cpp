#include <Stringsplit.h>
#include <fstream>
#include "FileConfigReader.h"

// input from sluisx.txt
// poort:555x
// doortype:normal  // of fastlock
// motortype:normal // of step

std::map<std::string, std::string> FileConfigReader::Read(std::string address) {
  std::map<std::string, std::string> returnConfig;
  std::string line;

  std::ifstream myConfigFile(address);
  if (myConfigFile.is_open()) {
    while (std::getline(myConfigFile, line)) {
      std::vector<std::string> splitressult = stringsplit::split(line, ':');
      if (splitressult.size() != 2) {
        throw std::invalid_argument("configuration file contains wrong format");
      }
      returnConfig[splitressult[0]] = splitressult[1];
    }
    myConfigFile.close();
  } else {
    throw std::invalid_argument("configuration file not found");
  }

  return returnConfig;
}
